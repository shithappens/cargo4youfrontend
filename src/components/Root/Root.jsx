import { Outlet } from "react-router-dom";
import Menu from "../Menu/Menu";
import React from "react";
const Root = () => <>
    <Menu/>
    <Outlet/>
</>

export default Root;