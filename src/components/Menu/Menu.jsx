import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import {
  Link
} from "react-router-dom";
const Menu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar expand="md" dark color="dark">
        <NavbarBrand>
          <Link to="/" className="nav-link" activeClassName="active">Cargo4You</Link>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem>
              <NavLink>
                <Link to="/track" className="nav-link" activeClassName="active">Track</Link>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink>
                <Link to="/create" className="nav-link" activeClassName="active">Create a package</Link>
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );

}

export default Menu;