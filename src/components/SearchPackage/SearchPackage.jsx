import React from "react";
import { Container, Row, Col, Input, InputGroup, Button } from 'reactstrap';

const SearchPackage = ({id, setId, showSummary}) =>
    <Container fluid>
        <Row className="mt-5 align-items-center">
            <Col />
            <Col>
                <h1 className="text-center">Tracking</h1>
                <InputGroup>
                    <Input type="text" placeholder="Enter the package reference" value={id} onChange={(e) => setId(e.target.value)} />
                    <Button color="info" onClick={() => showSummary()}>Track</Button>
                </InputGroup>
            </Col>
            <Col />
        </Row>
    </Container>

export default SearchPackage;