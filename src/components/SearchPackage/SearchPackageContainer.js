import SearchPackage from "./SearchPackage"
import { useNavigate } from 'react-router-dom';import React, {useState} from "react";
const SearchPackageContainer = () => {
    let history = useNavigate();
    const [id, setId] = useState(null);
    const showSummary = () => {
        history(id);
    };
    return <SearchPackage id={id} setId={setId} showSummary={showSummary}  />
}

export default SearchPackageContainer;