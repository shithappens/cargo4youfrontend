import React from "react";
import { Container, Col, Row, Input, Button, Label, FormGroup, Form } from 'reactstrap';

const PackageDetail = ({ packet, updatePacketState }) => <>
    <Row>
        <Col>
            Height
        </Col>
        <Col>
            <Input type="number" value={packet.height} onChange={(e) => updatePacketState('height', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Width
        </Col>
        <Col>
            <Input type="number" value={packet.width} onChange={(e) => updatePacketState('width', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Length
        </Col>
        <Col>
            <Input type="number" value={packet.length} onChange={(e) => updatePacketState('length', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Weigth
        </Col>
        <Col>
            <Input type="number" value={packet.weight} onChange={(e) => updatePacketState('weight', e.target.value)} />
        </Col>
    </Row>
</>

export default PackageDetail;
