import React from "react";
import {Row,Col} from 'reactstrap';
const SummaryDetail = ({packet, receiver, deliveryOptions}) => <>
    <Row>
        <Col>
            <h1>{packet.encodedId}</h1>
        </Col>
    </Row>
    <Row>
        <Col md={6}>
            Height : {packet.height}
        </Col>
        <Col md={6}>
            Width : {packet.width}
        </Col>
        <Col md={6}>
            Length : {packet.length}
        </Col>
        <Col md={6}>
            Weight : {packet.weight}
        </Col>
    </Row>
    <Row>
        <Col md={6}>
            Detail : {receiver.firstName} {receiver.lastName}
        </Col>
        <Col md={6}>
            Address : {receiver.address}
        </Col>
    </Row>
    <Row>
        <Col md={6}>
            Delivery : {deliveryOptions.find(elem => elem.id === packet.deliveryOptionId)?.price} EUR
        </Col>
    </Row>
</>

export default SummaryDetail;