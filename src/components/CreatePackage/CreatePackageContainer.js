import { fetchDeliveryOptions } from "../../slices/deliveryOptionSlice";
import { createPackage, setPackage, updatePackage } from "../../slices/packageSlice";
import { fetchReceiver, setReceiver, updateReceiver } from "../../slices/receiverSlice";
import { fetchSender, setSender, updateSender } from "../../slices/senderSlice";
import CreatePackage from "./CreatePackage";
import { connect } from 'react-redux';
import React, { useEffect, useState } from "react";
const CreatePackageContainer = ({
    generatePackage,
    updatePackage,
    setSender,
    updateReceiver,
    setReceiver,
    loadDeliveryOptions,
    loadReceiver,
    loadSender,
    packet,
    deliveryOptions,
    sender,
    receipient,
    setPacket,
    updateSender
}) => {
    const [isCreated, setIsCreated] = useState(false);
    const [isDetailLoaded, setIsDetailLoaded] = useState(false);
    const [currentPage, setCurrentPage] = useState(0);
    const updatePacketState = (key, value) => {
        let tmpPacket = { ...packet };
        tmpPacket[key] = value;
        setPacket(tmpPacket);
    };
    const updateReceiverState = (key, value) => {
        let tmpReceiver = { ...receipient };
        tmpReceiver[key] = value;
        setReceiver(tmpReceiver);
    };
    const updateSenderState = (key, value) => {
        let tmpSender = { ...sender };
        tmpSender[key] = value;
        setSender(tmpSender);
    }
    const nextPage = () => {
        setCurrentPage(currentPage + 1);
    };
    const previousPage = () => {
        if (currentPage > 0) {
            setCurrentPage(currentPage - 1);
        }
    }

    useEffect(() => {
        if (!isCreated) {
            generatePackage();
            setIsCreated(true);
        }
    }, []);
    useEffect(() => {
        if (packet.encodedId && !isDetailLoaded) {
            loadReceiver(packet.encodedId);
            loadSender(packet.encodedId);
            setIsDetailLoaded(true);
        }
    }, [packet]);
    useEffect(() => {
        switch (currentPage) {
            case 1:
                updatePackage(packet);
                break;
            case 2:
                updateSender(packet.encodedId, sender);
                break;
            case 3:
                updateReceiver(packet.encodedId, receipient);
                loadDeliveryOptions(packet.encodedId);
                break;
            case 4:
                updatePackage(packet);
                break;
            default:
                break;
        }
    }, [currentPage]);
    return <CreatePackage packet={packet} deliveryOptions={deliveryOptions} currentPage={currentPage} nextPage={nextPage} previousPage={previousPage} receipient={receipient} sender={sender} updatePacketState={updatePacketState} updateReceiverState={updateReceiverState} updateSenderState={updateSenderState} />
};
const mapStateToProps = (state, ownProps = {}) => {
    const { packet, deliveryOptions, sender, receiver } = state;
    return { packet, deliveryOptions, sender, receipient : receiver };
}
const mapDispatchToProps = (dispatch) => {
    return {
        // explicitly forwarding arguments
        generatePackage: () => dispatch(createPackage()),

        // implicitly forwarding arguments
        updatePackage: (packet) =>
            dispatch(updatePackage(packet)),
        setPacket: (packet) => dispatch(setPackage(packet)),
        updateSender: (id, sender) => dispatch(updateSender(id, sender)),
        setSender: (sender) => dispatch(setSender(sender)),
        updateReceiver: (id, receiver) => dispatch(updateReceiver(id, receiver)),
        setReceiver: (receiver) => dispatch(setReceiver(receiver)),
        loadDeliveryOptions: (packageId) => dispatch(fetchDeliveryOptions(packageId)),
        loadReceiver: (packageId) => dispatch(fetchReceiver(packageId)),
        loadSender: (packageId) => dispatch(fetchSender(packageId)),

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreatePackageContainer);