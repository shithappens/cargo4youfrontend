import { fetchDeliveryOptions } from "../../slices/deliveryOptionSlice";
import { fetchPackage } from "../../slices/packageSlice";
import { fetchReceiver} from "../../slices/receiverSlice";
import { fetchSender} from "../../slices/senderSlice";
import { connect } from 'react-redux';
import React, { useEffect, useState } from "react";
import SummaryDetail from "./SummaryDetail";
import {Route, Link, Routes, useParams} from 'react-router-dom';
const CreatePackageContainer = ({
    loadDeliveryOptions,
    loadReceiver,
    loadSender,
    loadPackage,
    packet,
    deliveryOptions,
    sender,
    receipient,
}) => {
    const params = useParams();
    useEffect(() => {
        if(params.id)
        {
            loadPackage(params.id);
        }
    }, params)
    const [isDetailLoaded, setIsDetailLoaded] = useState(false);
    useEffect(() => {
        if (packet.encodedId && !isDetailLoaded) {
            loadReceiver(packet.encodedId);
            loadSender(packet.encodedId);
            loadDeliveryOptions(packet.encodedId);
            setIsDetailLoaded(true);
        }
    }, [packet]);
    return <SummaryDetail packet={packet} deliveryOptions={deliveryOptions} receiver={receipient} sender={sender} />
};
const mapStateToProps = (state, ownProps = {}) => {
    const { packet, deliveryOptions, sender, receiver } = state;
    return { packet, deliveryOptions, sender, receipient : receiver };
}
const mapDispatchToProps = (dispatch) => {
    return {
        loadPackage: (packageId) => dispatch(fetchPackage(packageId)),
        loadDeliveryOptions: (packageId) => dispatch(fetchDeliveryOptions(packageId)),
        loadReceiver: (packageId) => dispatch(fetchReceiver(packageId)),
        loadSender: (packageId) => dispatch(fetchSender(packageId)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreatePackageContainer);