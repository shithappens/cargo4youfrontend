import React from "react";
import { Container, Col, Row, Input, Button, Label, FormGroup, Form } from 'reactstrap';
import DeliveryOptionDetail from "./DeliveryOptionDetail";
import PackageDetail from "./PackageDetail";
import ReceiverDetail from "./ReceiverDetail";
import SenderDetail from "./SenderDetail";
import SummaryDetail from "./SummaryDetail";
const CreatePackage = ({ packet, deliveryOptions, currentPage, nextPage, previousPage, receipient, sender, updatePacketState, updateReceiverState, updateSenderState }) =>
    <Container>
        {currentPage === 0 && <PackageDetail packet={packet} updatePacketState={updatePacketState} />}
        {currentPage === 1 && <SenderDetail sender={sender} updateSenderState={updateSenderState} />}
        {currentPage === 2 && <ReceiverDetail receipient={receipient} updateReceiverState={updateReceiverState} />}
        {currentPage === 3 && <DeliveryOptionDetail packet={packet} deliveryOptions={deliveryOptions} updatePacketState={updatePacketState} />}
        {currentPage === 4 && <SummaryDetail packet={packet} deliveryOptions={deliveryOptions} receiver={receipient} sender={sender} />}
        <Row>
            <Col>
                {currentPage > 0 && <Button color="secondary" size="lg" onClick={() => previousPage()} block>Previous</Button>}
            </Col>
            <Col>
                {currentPage < 4 && <Button color="info" size="lg" onClick={() => nextPage()} block>Next</Button>}


            </Col>
        </Row>
    </Container>

export default CreatePackage;