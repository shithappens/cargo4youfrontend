import React from "react";
import { Container, Col, Row, Input, Button, Label, FormGroup, Form } from 'reactstrap';

const DeliveryOptionDetail = ({ packet, deliveryOptions, updatePacketState }) => <>
    {
        deliveryOptions.map(element =>
            <Row>
                <Col>
                    <Button color={packet.deliveryOptionId === element.id ? "info" : "secondary"} onClick={() => updatePacketState('deliveryOptionId', element.id)}> {element.price} EUR </Button>
                </Col>
            </Row>
        )
    }
    {/* deliveryOptions.map(element => <Button color={packet.deliveryOptionId === element.id ? "info" : "secondary"} onClick={() => updatePacketState('deliveryOptionId', element.id)}> {element.price} EUR </Button>); */}

</>

export default DeliveryOptionDetail;
