import React from "react";
import { Container, Col, Row, Input, Button, Label, FormGroup, Form } from 'reactstrap';

const SenderDetail = ({sender, updateSenderState}) => <>
    <Row>
        <Col>
            Firstname
        </Col>
        <Col>
            <Input type="text" value={sender.firstName} onChange={(e) => updateSenderState('firstName', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Lastname
        </Col>
        <Col>
            <Input type="text" value={sender.lastName} onChange={(e) => updateSenderState('lastName', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Email
        </Col>
        <Col>
            <Input type="email" value={sender.email} onChange={(e) => updateSenderState('email', e.target.value)} />
        </Col>
    </Row>
</>

export default SenderDetail;