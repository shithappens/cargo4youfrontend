import React from "react";
import { Container, Col, Row, Input, Button, Label, FormGroup, Form } from 'reactstrap';

const ReceiverDetail = ({receipient, updateReceiverState}) => <>
    <Row>
        <Col>
            Firstname
        </Col>
        <Col>
            <Input type="text" value={receipient.firstName} onChange={(e) => updateReceiverState('firstName', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Lastname
        </Col>
        <Col>
            <Input type="text" value={receipient.lastName} onChange={(e) => updateReceiverState('lastName', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Address
        </Col>
        <Col>
            <Input type="text" value={receipient.address} onChange={(e) => updateReceiverState('address', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            City
        </Col>
        <Col>
            <Input type="text" value={receipient.city} onChange={(e) => updateReceiverState('city', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Postalcode
        </Col>
        <Col>
            <Input type="text" value={receipient.postalCode} onChange={(e) => updateReceiverState('postalCode', e.target.value)} />
        </Col>
    </Row>
    <Row>
        <Col>
            Country
        </Col>
        <Col>
            <Input type="text" value={receipient.country} onChange={(e) => updateReceiverState('country', e.target.value)} />
        </Col>
    </Row>
</>

export default ReceiverDetail;