import { configureStore } from '@reduxjs/toolkit'
import packageSlice from './slices/packageSlice';
import receiverSlice from './slices/receiverSlice';
import senderSlice from './slices/senderSlice';
import deliveryOptionSlice from './slices/deliveryOptionSlice';
export const store = configureStore({
  reducer: {
    packet: packageSlice,
    receiver: receiverSlice,
    sender: senderSlice,
    deliveryOptions: deliveryOptionSlice
  },
})