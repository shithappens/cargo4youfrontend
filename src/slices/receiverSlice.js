import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
const baseUrl = "https://localhost:44368/api/Packages/";
const initialState = {
  packageId: 0,
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  address: "",
  city: "",
  postalCode: "",
  country: ""
}

export const receiverSlice = createSlice({
  name: 'receiver',
  initialState,
  reducers: {
    setReceiver: (state, action) => {
      return {...action.payload};
    }
  },
})
export const fetchReceiver = packageId => async dispatch => {
  const response = await axios.get(`${baseUrl}${packageId}/receiver`);
  dispatch(setReceiver(response.data))
}
export const updateReceiver = (id, receiver) => async dispatch => {
  await axios.put(`${baseUrl}${id}/receiver`, receiver);
}
// Action creators are generated for each case reducer function
export const { setReceiver } = receiverSlice.actions

export default receiverSlice.reducer