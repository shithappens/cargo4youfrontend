import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
const baseUrl = "https://localhost:44368/api/Packages/";
const initialState = {
  packageId: 0,
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  address: "",
  city: "",
  postalCode: "",
  country: ""
}

export const senderSlice = createSlice({
  name: 'sender',
  initialState,
  reducers: {
    setSender: (state, action) => {
        return {...action.payload};
    }
  },
})
export const fetchSender = packageId => async dispatch => {
  const response = await axios.get(`${baseUrl}${packageId}/sender`);
  dispatch(setSender(response.data))
}
export const updateSender = (id,sender) => async dispatch => {
  await axios.put(`${baseUrl}${id}/sender`, sender);
}
// Action creators are generated for each case reducer function
export const { setSender } = senderSlice.actions

export default senderSlice.reducer