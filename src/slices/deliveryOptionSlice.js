import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
const baseUrl = "https://localhost:44368/api/Packages/";
const initialState = []

export const deliveryOptionSlice = createSlice({
  name: 'deliveryOptions',
  initialState,
  reducers: {
    setDeliveryOptions: (state, action) => {
      return action.payload;
    }
  },
})
export const fetchDeliveryOptions = packageId => async dispatch => {
  const response = await axios.get(`${baseUrl}${packageId}/deliveryOptions`);
  dispatch(setDeliveryOptions(response.data))
}

// Action creators are generated for each case reducer function
export const { setDeliveryOptions } = deliveryOptionSlice.actions

export default deliveryOptionSlice.reducer