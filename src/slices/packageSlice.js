import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
const baseUrl = "https://localhost:44368/api/Packages/";
const initialState = {
    encodedId: "",
    note: "",
    width: 0,
    height: 0,
    length: 0,
    weight: 0,
    price: 0,
    deliveryPartnerRef: "",
    payedAt: null,
    createdAt: null,
    modifiedAt: null,
    deletedAt: null,
    deliveryOptionId: 0,
    deliveryOption: {}
}

export const packageSlice = createSlice({
    name: 'packet',
    initialState,
    reducers: {
        setPackage: (state, action) => {
            return {...action.payload};
        }
    },
})
export const fetchPackage = packageId => async dispatch => {
    const response = await axios.get(`${baseUrl}${packageId}`);
    dispatch(setPackage(response.data))
}
export const createPackage = () => async dispatch => {
    const response = await axios.post(`${baseUrl}`);
    dispatch(setPackage(response.data))
}
export const updatePackage = (data) => async dispatch => {
    await axios.put(`${baseUrl}${data.encodedId}`, data);
}
// Action creators are generated for each case reducer function
export const { setPackage } = packageSlice.actions

export default packageSlice.reducer