import {
    createBrowserRouter,
} from "react-router-dom";
import CreatePackage from "./components/CreatePackage/CreatePackage";
import CreatePackageContainer from "./components/CreatePackage/CreatePackageContainer";
import SummaryDetailContainer from "./components/CreatePackage/SummaryDetailContainer";
import PackageDetailContainer from "./components/PackageDetail/PackageDetailContainer";
import Root from "./components/Root/Root";
import SearchPackageContainer from "./components/SearchPackage/SearchPackageContainer";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        children: [
            {
                path: "/track/:id",
                element: <SummaryDetailContainer />,
            },
            {
                path: "/track",
                element: <SearchPackageContainer />
            },
            {
                path: "/create",
                element: <CreatePackageContainer />,
            },
            

        ]
    }
]);
export default router;